# AngularApplicationProgramming_book
    앵귤러 어플리케이션 프로그래밍 책 공부

https://wings.msn.to/index.php/-/A-03/978-4-7741-7568-3/

# AngularNotes
    아래 사이트 연습 및 로그인 관련해서 MaterialUI, FormBuilder 사용

https://developer.okta.com/blog/2019/04/26/angular-mvc-primer

# AngularRXJS_basic_exercises
    rxjs 기본 연습(아래 사이트 참조)

https://poiemaweb.com/angular-rxjs

# Angular_NavigationBar_test
    네비게이션바 + 컨텐츠 구조 테스트
    
    * app.component.html
    <app-main></app-main>

    * main.component.ts
    @Component({
        selector: 'app-main',

    * main.component.html
    <div>
    <!-- 메인 컨텐츠 -->
    <app-nav></app-nav>  <!-- 네비게이션바 -->

    <main role="main">
        <app-content></app-content> <!-- ContentsComponent -->
    </main>
    </div>

    * contents.component.ts
    @Component({
        selector: 'app-content',

    * contents.component.html
    <router-outlet></router-outlet> <!-- 라우팅 되는 컴포넌트를 담음 -->

# AngularGlobalErrorHander
    전역 에러 처리 아래 사이트 참고함
    서버 fetch 외에 offline 상태의 경우, indexedDB 저장(재전송) 처리 추가  
    stacktrace-js,
    Dexie(indexedDB)

https://golb.hplar.ch/2018/10/global-errorhandler-angular.html

# AngularAuthLocalStorage_Intercepter
    아래 사이트 참고함
    로그인정보 localstorage 저장 및 http Intercpeter 예제
    http intercepter는 app.module.ts의 providers 선언 순서에 따라 intercepter 됨

https://jasonwatmore.com/post/2018/05/23/angular-6-jwt-authentication-example-tutorial

# AngularJWT
    아래 사이트 참고
    JWT  관련해서 angular-memo.md 참고    

https://jasonwatmore.com/post/2019/06/22/angular-8-jwt-authentication-example-tutorial

# AngularRoutePrac
    실제 구현 대상 웹 어플리케이션 기본 구조 연습 (컴포넌트 담는 형식, 라우팅 등)
