import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { 
    MatMenuModule,
    MatButtonModule
} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main.component';
import { ContentsComponent } from './contents.component';
import { HeaderComponent, FooterComponent } from './cmnComp';
import { HomeComponent, CompanyComponent } from './webhome';
import { DashComponent } from './dashboard';

@NgModule({
    declarations: [
        AppComponent,
        MainComponent,
        ContentsComponent,
        HeaderComponent,
        FooterComponent,
        HomeComponent,
        CompanyComponent,
        DashComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        MatMenuModule,
        MatButtonModule,
        BrowserAnimationsModule
    ],
    exports:[
        MainComponent
    ],
    providers: [

    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
