import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent, CompanyComponent } from './webhome';
import { DashComponent } from './dashboard';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'company', component: CompanyComponent },
    { path: 'dash', component: DashComponent, canActivate: [] },
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }