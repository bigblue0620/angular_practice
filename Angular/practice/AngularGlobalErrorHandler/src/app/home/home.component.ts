import { Component } from '@angular/core';

@Component({
    selector:'app-home',
    templateUrl:'./home.component.html'
})

export class HomeComponent
{
    onClick():any{
        //TODO error 정보 형태
        var obj = {name:'home.component.ts error', test:'aa'};
        throw new Error(JSON.stringify(obj));
    }
}