import Dexie from 'dexie';

export interface ClientError{
    id?:number, //optional property denoted ?
    error:string
};

export class ClientErrorDb extends Dexie
{
    errTbl:Dexie.Table<ClientError, string>;
    
    constructor()
    {
        super('ClientErrors'); //ClientErrors == IndexcedDB databaseName
        this.version(1).stores({
            errTbl: '++id'
        });
    }
}