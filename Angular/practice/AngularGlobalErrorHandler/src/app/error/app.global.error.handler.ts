import { ErrorHandler, Injectable } from '@angular/core';
import * as StackTrace from 'stacktrace-js';
import { ClientErrorService } from './client.error.service';

@Injectable()
export class AppGlobalErrorhandler implements ErrorHandler
{
    private isRetryRunning = false;
    serverURL:string = `http://localhost:65535/clientError`;

    constructor(private readonly clientErrorService:ClientErrorService){
        this.sendStoredErrors();
        window.addEventListener('online', () => this.sendStoredErrors());
    }

    async handleError(error:any){
        //console.log(error);

        const userAgent = {
            language: navigator.language,
            platform: navigator.platform,
            userAgent: navigator.userAgent,
            connectionDownlink: navigator['connection'].downlink,
            connectionEffectiveType: navigator['connection'].effectiveType
        }

        const stackTrace = await StackTrace.fromError(error, {offline:true});
        const body = JSON.stringify({ts:new Date().toISOString(), userAgent, stackTrace});

        /*
        fetch(`127.0.0.1:65535/clientError`, {
            method: 'POST',
            body: `[${body}]`,
            headers: {
              'content-type': 'application/json'
            }
          }).then((res)=> console.log("error fetch succeeed"))
            .catch((error) => console.log('error fetch error happened : ' + error.message);)
         */

        //TODO 서버 통신이 안되는 경우 indexedDB에 저장하는 처리 추가(필요성 논의)

        const wasOK = await this.sendError(body);
        if(!wasOK){
            this.clientErrorService.store(body);
            setTimeout(() => this.sendStoredErrors(), 60_000); //60초=1분
        }
    }

    private async sendError(errors:string[]|string):Promise<boolean>
    {
        if(navigator.onLine){
            try{
                let body = Array.isArray(errors) ? `[${errors.join('')}]` : `[${errors}]`;
            
                const res = await fetch(this.serverURL, {
                    method: 'POST',
                    body: `[${body}]`,
                    headers: {
                        'content-type': 'application/json'
                    }
                });

                if(res.ok){
                    return true;
                }
            }catch(error){
                console.log(error);
            }
        }

        return false;
    }

    private async sendStoredErrors():Promise<void>
    {
        if(this.isRetryRunning){
            return;
        }

        let attempts = 1;

        const retry = async () => {
            const errors = await this.clientErrorService.getAll();
            if(errors.length === 0){
                return;
            }

            const wasOK = await this.sendError(errors.map(error => error.error));
            if(wasOK){
                await this.clientErrorService.delete(errors.map(error => error.id));
                this.isRetryRunning = false;
                return;
            }

            this.isRetryRunning = true;
            if(attempts < 32){
                attempts = attempts * 2;
            }

            setTimeout(retry, attempts * 60_000);
        }

        await retry();
    }
}