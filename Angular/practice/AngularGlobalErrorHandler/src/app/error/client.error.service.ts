import { ClientError, ClientErrorDb} from './client.error.db'
import { Injectable } from '@angular/core';
import Dexie from 'dexie';

@Injectable({
    providedIn: 'root'
})
export class ClientErrorService 
{
    private db:ClientErrorDb

    constructor(private clientErrDb:ClientErrorDb){
        this.db = new ClientErrorDb();
    }

    async store(body:string):Promise<void>{
        await this.db.errTbl.add({error:body});
    }

    async delete(idArr:number[]):Promise<void>{
        await this.db.errTbl.bulkDelete(idArr);
    }

    async getAll():Promise<ClientError[]>{
        return await this.db.errTbl.toArray();
    }
}