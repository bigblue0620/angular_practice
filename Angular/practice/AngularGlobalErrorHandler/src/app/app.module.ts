import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';

import { AppGlobalErrorhandler } from './error/app.global.error.handler';
import { HomeComponent } from './home/home.component';
import { ClientErrorDb } from './error/client.error.db';
import { ClientErrorService } from './error/client.error.service';

const routes:Routes = [
    {path:'', redirectTo:'home', pathMatch:'full'},
    {path:'home', component:HomeComponent}
]

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        RouterModule.forRoot(routes, {useHash:true}),
        
    ],
    providers: [
        {provide:ErrorHandler, useClass:AppGlobalErrorhandler},
        ClientErrorDb,
        ClientErrorService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
