# Angular 메모, 실수 정리

#### Uncaught Error: Template parse errors:Can't bind to 'formControl' since it isn't a known property of 'input'
##### 발생 원인 : App module에 component 만 정리해둔 모듈을 인포트할 때 component에서 ReactiveFormsModule 등을 쓸 때는 App Module 이 아니라  component 모듈에 넣어둬야 함 / 또는 컴포넌트들을 다 app module에 정의하면 됨

     app.module.ts
     component.module.ts

    --- app.module.ts
     import { ComponentModule } from './component.module'; //컴포넌트들만 모아둔 모듈

    --- component.module.ts  // 컴포넌트에서 만약 ReactiveFormsModule을 쓴다면
    //이 구문은 component.module.ts에 넣어야 인식함
    import { ReactiveFormsModule } from '@angular/forms'; 

    만약 app.module.ts에 ReactiveFormsModule을 넣어둔다면 아래

    Template parse errors: Can't bind to 'formControl' since it isn't a known property
    또는 valueChanges가 동작하지 않음

#### Uncaught Error: Template parse errors:The pipe 'json' could not be found
##### 발생 원인 : Can't bind to 'formControl' 과 동일
    
    component.module.ts 에 
    import { CommonModule } from '@angular/common'; 넣고
    아래에 추가
    imports: [
        ReactiveFormsModule,
        CommonModule

#### 폼 에러 로그 찍기

    Object.keys(this.regEditForm.controls)
        .forEach(key => 
            {
                const controlErrors: ValidationErrors = this.regEditForm.get(key).errors;
                if (controlErrors != null) {
                    Object.keys(controlErrors).forEach(keyError => {console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);});
                }
            }
    );

#### 라우팅
    주소이름은 맨 앞의 / 를 생략해서 씀.

    /는 빈 문자열
    { path: '', component: HomeComponent ~

    /config는 'config'
    { path: 'config', component: ConfigComponent ~

#### environments 설정
    tsconfig.json의 lib 밑에 아래 추가

    "paths": {
      "@environments/*": ["src/environments/*"]
    }

    참조할 파일(예: auth.service.ts)에서 아래와 같이, 근데 VSCode에서 Cannot find module 에러남, ng serve 컴파일 시에는 OK
    import { environment } from '@environments/environment';

#### JWT(JSON Web Token) 

    JWT 는 . 을 구분자로 3가지의 문자열
    aaaaaaaaaaaaa.bbbbbbbbbbbbbbb.cccccc
    헤더(header)   내용(payload)   서명(signature)

    JWT 토큰 자체는 서버에서 만들어서 클라이언트가 받은 후(보통 로그인)
    클라이언트에서 서버 요청시 JWT를 Authorization Header에 지정함.
   
    ex) setHeaders: {
            Authorization: `Bearer ${currentUser.token}`
        }

https://idlecomputer.tistory.com/240
https://swalloow.github.io/implement-jwt  (JWT Process 참고)
https://medium.com/@mjkim111/json-web-token-jwt-%EC%A0%95%EB%A6%AC-abbc80570301
https://jwt.io/ (Tool)

#### pipe(first()) 쓰는 이유
    Observable 변수를 사용 할 때, (계속 동작할 수 있으므로) Observable의 사용이 끝났을 때, 중지(unsubscribe)하는 것이 필요
    first()는 한 번 구독하고 중지,  주로 API 요청을 할 때,first 를 사용함.
    take(실행횟수)는 최대로 수행할 횟수를 지정함.

https://ddalpange.github.io/2018/11/21/how-to-unsubscibe-in-rxjs/   

#### app.module.ts

    * entryComponents: [
        RegEditComponent //팝업 등
      ],

    * exports : [
        MainComponent
      ],
        
        declarations에서 정의한 현재 모듈의 구성요소를 다른 모듈의 템플릿에서 사용할 수 있도록 노출시킬
        view class들을 지정.(여기서 지정하지 않은 뷰 클래스들은 다른 모듈에서 사용 불가)
        예를 들어 MainComponent가 다른 컴포넌트를 담는 그릇같은 용도라면 exports에 선언해 줘야 함.

#### Can't resolve all parameters for MemberService: ([object Object], ?, [object Object], [object Object]).
    이런 에러가 떴을 경우에는 app.module.ts 등등에 해당하는 서비스를 providers에 포함시켰는지
    또는 @Inject 데코레이터로 constructor에 주입했는지 등등 체크하기

#### ERROR Error: StaticInjectorError(AppModule)[UserformService -> HttpClient]:
    app.module.ts의 imports에 HttpClientModule 포함유무 체크하기

#### this.router.navigate(['./dashboard/extractdata'], {skipLocationChange: true, state: {data: {contents:t ....
    skipLocationChange를 true로 하면 url이 변경되어도 실제 url 창에는 이전 url이 표시됨(즉 url이 변경되지 않음)

#### Could not find HammerJS. Certain Angular Material components may not work correctly.
    npm install --save hammerjs 한 후
    import 'hammerjs'; (app's entry point (e.g. src/main.ts))

#### ExpressionChangedAfterItHasBeenCheckedError
    setTimeout(()=> {,,,}, 0)으로 처리를 비동기시키면 해소

https://medium.com/sjk5766/expressionchangedafterithasbeencheckederror-%EC%97%90%EB%9F%AC%EC%97%90-%EB%8C%80%ED%95%B4-79dac955cfa1
