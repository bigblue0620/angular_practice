# Angular RxJS

> https://poiemaweb.com/angular-rxjs

## 리액티브 프로그램밍
###  다양한 데이터를 데이터 스트림이라는 하나의 일관된 형식으로 만들고, 이 데이터 스트림을 구독(subscribe)하여 데이터 스트림의 상태 변화에 반응하는 방식으로 동작

### 애플리케이션이 필요한 데이터를 외부 환경에 요청하는 것이 아니라, 외부 환경을 관찰하고 있다가 외부 환경에서 데이터 스트림을 방출하면 그것에 반응하여 데이터를 획득  

### 옵저버블(Observable)
#### 외부 환경에서 애플리케이션 내부로 연속적으로 흐르는 데이터, 즉 데이터 스트림을 생성하고 방출하는 객체

### 옵저버(Observer)
#### 옵저버블이 방출한(emit) 노티피케이션(Notification: 옵저버블이 방출할 수 있는 푸시 기반 이벤트 또는 값)을 획득하여 사용하는 객체

    데이터 소비자(Data consumer)인 옵저버는 데이터 생산자(Data producer)인 옵저버블을 구독(subscription)

    구현의 관점에서 구독이란 옵저버블의 subscribe 오퍼레이터를 호출하는 것. 옵저버블이 생성한 데이터 스트림을 subscribe 오퍼레이터로 구독하면 Subscription 객체를 반환
    this.subscription = observable$.subscribe(


## RxJS

### 비동기 데이터 스트림을 처리하는 API를 제공하는 자바스크립트 라이브러리

