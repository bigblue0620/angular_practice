import {Component, AfterContentChecked, ContentChild} from '@angular/core';
import {ChildComponent} from './child.component';

@Component({
    selector:'my-parent',
    template:`
            <ng-content></ng-content>
            <hr/>
            completed : {{poem}}
            `
})

export class ParentComponent{
    @ContentChild(ChildComponent) child:ChildComponent;
    poem:string = '';

    ngAfterContentChecked():void{
        console.log("-------ngAfterContentChecked------");
        if(this.poem !== this.child.poem){
            this.poem = this.child.poem;
        }
    }
}