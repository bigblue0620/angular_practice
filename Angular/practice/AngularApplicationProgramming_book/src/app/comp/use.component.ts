import {Component, OnInit} from '@angular/core';
import {UseService} from '../service/use.service';

@Component({
    selector:'my-use',
    template:`
             <div>UseService : {{current}}</div>
            `,
    //providers : [{provide:UseService, useClass:UseService}] //주입시마다 새로운 클래스 인스턴스 생성
    providers : [{provide:UseService, useValue:new UseService()}] //미리 생성된 클래스 인스턴스를 넘겨줌
})

export class UseComponent implements OnInit
{
    current:String = '';

    constructor(private use:UseService){}

    ngOnInit():void{
        this.current = this.use.show();
    }
}