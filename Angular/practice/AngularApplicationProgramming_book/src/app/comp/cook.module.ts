import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import { DetailsComponent }  from './details.component';
import { EditComponent }  from './edit.component';
import { ChildComponent } from './child.component';
import { ContentComponent } from './content.component';
import { ParentComponent } from './parent.component';

@NgModule({
    imports: [ CommonModule, FormsModule ],
    declarations: [DetailsComponent, EditComponent, ChildComponent, ContentComponent, ParentComponent],
    exports: [DetailsComponent, EditComponent, ChildComponent, ContentComponent, ParentComponent]
})

export class CookModule{}