import {Component, Input, Output, EventEmitter, isDevMode} from '@angular/core';
import {Book} from '../class/book';

@Component({
    selector: 'detail-book',
    template: `
            <div *ngIf='item'>
                <ul>
                    <li>ISBN : {{item.isbn}}</li>
                    <li>Title : {{item.title}}</li>
                    <li>Price : {{item.price}}</li>
                    <li>Publisher : {{item.publisher}}</li>
                </ul>
                <button (click)='onmodify()'>modify</button>
            </div>
            `
})

export class DetailsComponent{
    //@Input() item:Book;
    //or getter/setter

    @Input() isModify:boolean;
    @Output() modify = new EventEmitter<boolean>();

    private _item:Book;

    @Input()
    set item(_item:Book){
        this._item = _item;
    }

    get item():Book{
        return this._item;
    }

    onmodify():void{
        this.modify.emit(true);
    }
}