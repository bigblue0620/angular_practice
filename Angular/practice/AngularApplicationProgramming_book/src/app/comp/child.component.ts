import { Component, Input } from '@angular/core';

@Component({
  selector: 'my-child',
  template: `
    <div>
      三行詩{{index}}：<input name="poem" [(ngModel)]="poem" size="20" />
    </div>
    <p>test</p>
    <h2>h2 test</h2>
  `,
})
export class ChildComponent {
  //@Input() index: number; 
  poem: string;
}