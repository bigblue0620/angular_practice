import {Component} from '@angular/core';

@Component({
    selector:'my-content',
    template:`
            <ng-container>
                <div>Hello, <ng-content></ng-content> !</div>
                <ng-content select='.header'></ng-content>
                <ng-content select='small'></ng-content>
            </ng-container>
            `
})
export class ContentComponent {}