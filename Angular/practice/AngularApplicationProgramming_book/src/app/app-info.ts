import {InjectionToken} from '@angular/core';

export const MY_APP_INFO = {
    title : 'Angular Sample',
    auther : 'YAMADA',
    create : new Date(2017, 2,14)
}

export let APP_INFO = new InjectionToken('app info');