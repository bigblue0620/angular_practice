import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {MainComponent}  from './route/main.component';
import {ExampleComponent} from "./route/example.component";
import {ErrorComponent} from './route/error.component';
import {ArticleComponent} from './route/article.component';
import {ParamComponent} from './route/param.component';
import {SearchComponent} from './route/search.component';

import {BookService}  from './service/book.service';
//import {UseComponent} from './comp/use.component';

import {TrimPipe} from './pipe/trim.pipe';

import {MY_ROUTES} from './app.routing';

@NgModule({
  imports:      [BrowserModule, FormsModule, HttpModule, JsonpModule, MY_ROUTES],
  providers:    [BookService,],
  declarations: [AppComponent, MainComponent, ExampleComponent, ErrorComponent, ArticleComponent, ParamComponent, SearchComponent, TrimPipe],//현재 모듈에 속하는 컴포넌트 등
  entryComponents:[], //동적인포트하는 컴포넌트 선언
  bootstrap:    [AppComponent] //app에서 최초 기동해야할 최상위 컴포넌트
})
export class AppModule { }
