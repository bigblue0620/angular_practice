
import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector:'my-app',
    template:`
            <ul>
                <li><a routerLink='/' routerLinkActive='current' [routerLinkActiveOptions]='{exact:true}'>main page</a></li>
                <li><a [routerLink]="['/exam', {outlets: {'other': null}}]" routerLinkActive='current'>Example Page</a></li>
                <li><a routerLink='/dummy' routerLinkActive='current'>Invalid Page</a></li>
                <li><a [routerLink]='["article", "13"]' routerLinkActive='current'>Article No.13</a></li>
                <li><a routerLink='/article/100' routerLinkActive='current'>Article No.100</a></li>
                <li><a routerLink='/param' routerLinkActive='current' [queryParams]='{category:"angular", keyword:"Routing"}' fragment='hoge'>Query/Fragment</a></li>
                <!--
                <li><a [routerLink]='[{outlets:{primary:["main"], other:["search", "Angular"]}}]' routerLinkActive='current'>Angular/Karma/Rx</a></li>
                -->
                <!--
                <li><a routerLink='/search/Angular/Karma/Rx' routerLinkActive='current'>Angular/Karma/Rx</a></li>
                -->
            </ul>
            <hr/>
            <router-outlet></router-outlet>
            <hr/>
            <router-outlet name='other'></router-outlet>
            <hr/>
            <button (click)='onclick()'>Test</button>
            <hr/>
            <p>[{{msg}}]</p>
            <p>[{{msg|trim}}]</p>
            `,
    styles:[
        `.current {background-color:#ff0}
         p {white-space:pre}
        `
    ],
    providers:[]
})

export class AppComponent{
    msg:string = '   Wings Project   ';

    constructor(private router:Router){}

    onclick(){
        this.router.navigate(['/exam']);
    }
}

//<li><a routerLink='/search/Angular/Karma/Rx' routerLinkActive='current'>Angular/Karma/Rx</a></li>
