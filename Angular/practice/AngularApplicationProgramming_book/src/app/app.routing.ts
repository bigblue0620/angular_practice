import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { MainComponent }  from './route/main.component';
import { ExampleComponent } from "./route/example.component";
import { ErrorComponent } from './route/error.component';
import { ArticleComponent } from './route/article.component';
import { ParamComponent } from './route/param.component';
import { SearchComponent } from './route/search.component';

const myRoutes = [
    //{path:'', children:[{path:'', component:MainComponent}, {path:'', component:SearchComponent, outlet:'other'}]},
    {path:'main', component:MainComponent},
    {path:'exam', component:ExampleComponent},
    {path:'search/:id', component:SearchComponent, outlet:'other'},
    {path:'param', component:ParamComponent, data:{name:'route param'}},
    {path:'article/:id', component:ArticleComponent},
    {path:'', redirectTo:'/main(other:search/test2)', pathMatch:'full'}
    {path:'**', component:ErrorComponent}
    //{path:'**', redirectTo:'/'}
]

export const MY_ROUTES:ModuleWithProviders = RouterModule.forRoot(myRoutes);