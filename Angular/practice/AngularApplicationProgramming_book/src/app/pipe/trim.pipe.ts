import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name:'trim'})

export class TrimPipe implements PipeTransform
{
    transform(value:string){
        return (typeof value !== 'string') ? value : value.trim();
    }   
}