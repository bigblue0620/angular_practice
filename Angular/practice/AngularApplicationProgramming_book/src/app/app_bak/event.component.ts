import { Component } from '@angular/core';

@Component({
    selector: 'my-event',
    template: `
               <div class='banner event'>
                <h3>Event Component</h3>
                <p>Come on</p>
               </div>
              `,
    styleUrls: ['app/app.component.css']
})
export class EventComponent{}