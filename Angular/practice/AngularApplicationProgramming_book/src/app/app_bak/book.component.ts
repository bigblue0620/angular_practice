import { Component } from '@angular/core';

@Component({
    selector: 'my-book',
    template: `
               <div class='banner book'>
                <h3>Book component</h3>
                <p>Book Book</p>
               </div>
              `,
    styleUrls: ['app/app.component.css']
})
export class BookComponent{}