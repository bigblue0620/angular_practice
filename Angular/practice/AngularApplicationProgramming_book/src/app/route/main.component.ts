import { Component } from '@angular/core';

@Component({
  template: `
    <div class="component">
      <h2>Main Page</h2>
      <img src="http://www.wings.msn.to/image/wings.jpg" alt="WINGS Logo" />
    </div>
  `
})
export class MainComponent { }
