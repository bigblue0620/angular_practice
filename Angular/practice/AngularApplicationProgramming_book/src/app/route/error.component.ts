import { Component } from '@angular/core';

@Component({
  styles:["div h2{ background-color:#f00; }"],
  template: `
    <div class="component">
      <h2>Error</h2>
      <p>Page is missing</p>
    </div>
  `
})
export class ErrorComponent { }