import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
    template: `
      <div>
        <h1>Article Info No.{{ id }}</h1>
        <p>This Article's number is {{ id }}</p>
      </div>
    `
  })

  export class ArticleComponent implements OnInit
  {
      id:string = '';

      constructor(private route:ActivatedRoute){}

      ngOnInit(){
          this.route.params.subscribe(
              params => this.id = params['id']
          );
      }
  }