import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
    template: `
      <ul>
        <li>keyword : {{keyword}}</li>
      </ul>
    `
  })

  export class SearchComponent implements OnInit
  {
      keyword:string = '';

      constructor(private route:ActivatedRoute){}

      ngOnInit(){
          this.route.url.subscribe(strs => {
              this.keyword = strs.join(' ');
          })
      }
  }