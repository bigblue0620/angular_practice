import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from "@angular/router"

@Component({
    template: `
      <ul>
        <li>Query Info : {{query}}</li>
        <li>fragment : {{fragment}}</li>
        <li>other info : {{data}}</li>
      </ul>
    `
  })

export class ParamComponent implements OnInit
{
    query:string = '';
    fragment:string = '';
    data:string = '';

    constructor(private route:ActivatedRoute){}

    ngOnInit(){
        this.route.queryParams.subscribe(
            params => this.query = `${params['category']}/${params['keyword']}`
        );

        this.route.fragment.subscribe(frag => this.fragment = frag);

        this.route.data.subscribe(obj => this.data = obj['name']);
    }
  }
