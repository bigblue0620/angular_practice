import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
    <div class="component">
      <h2>Example</h2>
      <p>ExampleComponent</p>
    </div>
  `
})
export class ExampleComponent { }
