﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { User } from '../_models';

@Injectable({ providedIn: 'root' })
export class UserService {

    _loginData:any;

    constructor(private http: HttpClient) { }

    set loginData(d:any){
        this._loginData = d;
    }

    get loginData(){
        return this._loginData;
    }

    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/hello`);
    }
}