import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './_services';
import { User } from './_models';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    currentUser: User;

    constructor(private router:Router, private authSrv:AuthenticationService){
        this.authSrv.currentUser.subscribe(u => this.currentUser = u);
    }

    logout():void{
        this.authSrv.logout();
        this.router.navigate(['/login']);
    }
}