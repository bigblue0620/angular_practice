﻿import { Component } from '@angular/core';
import { first, take } from 'rxjs/operators';

import { User } from '../_models';
import { UserService } from '../_services';
//import * as jwt_decode from 'jwt-decode'; //JWTs token which are Base64Url encoded.

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    loading = false;
    users: User[];
    //jwt.io에서 하기 문자열 디코드 가능 //your-256-bit-secret == upenn //encoded 알고리즘 HS256
    jwt:string = '';
    refresh:string = '';
    loginData:any;

    constructor(private userService: UserService) {
        //this.decoded = jwt_decode(origin);
    }

    ngOnInit() {
        this.loading = true;
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.loading = false;
            this.users = users;
        });
    }
}