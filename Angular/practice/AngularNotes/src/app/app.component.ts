import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { LoginDialogComponent } from './login/login.dialog.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit
{
    title:string = 'Angular Notes';
    isAuthenticated:boolean;

    constructor(private dialog:MatDialog){
        this.isAuthenticated = false;
    }

    ngOnInit():void{

    }

    openDialog():void{
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = false;
        dialogConfig.autoFocus = true;
        dialogConfig.data = {id:'', pw:''};

        const dialogRef = this.dialog.open(LoginDialogComponent, dialogConfig);

        dialogRef.afterClosed().subscribe(
            data => console.log(data)
        );
    }

    logout():void{
        
    }
}