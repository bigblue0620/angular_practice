import { Component, OnInit, Inject, Optional } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'login-dialog',
    templateUrl: './login.dialog.component.html',
    styleUrls: ['./login.dialog.component.css']
})

export class LoginDialogComponent implements OnInit 
{
    loginForm:FormGroup;
    id:string;
    pw:string;

    constructor(private fb:FormBuilder, @Optional() private dialogRef:MatDialogRef<LoginDialogComponent>, @Optional() @Inject(MAT_DIALOG_DATA) data:any){
        //console.log(data);
        this.id = data.id;
        this.pw = data.pw;
    }

    ngOnInit():void{
        this.loginForm = this.fb.group({
            id:['', Validators.required],
            pw:['', Validators.required],
        })
    }

    login():void{
        if(!this.loginForm.valid) {
            return;
        }

        this.dialogRef.close(this.loginForm.value);

        //TODO 여기서 로그인 처리,  app component 에서 해도 되지만 ㄷㄷㄷㄷ


    }

    close():void{
        this.dialogRef.close();
    }
}    