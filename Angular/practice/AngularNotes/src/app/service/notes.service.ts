import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observer } from 'rxjs';
import { Note } from '../class/Note';
import { NoteInfo } from '../class/NoteInfo';

@Injectable({
  providedIn: 'root'
})

export class NotesService implements OnDestroy
{
    notes:Note[];
    nextId = 0;
    notesSubject = new BehaviorSubject<NoteInfo[]>([]);

    constructor(){ 
        this.notes = JSON.parse(localStorage.getItem('notes')) || [];
        for(const note of this.notes){
            if(note.id >= this.nextId){
                this.nextId = note.id + 1;
            }
        }
        this.update();
    }

    subscribe(observer:Observer<NoteInfo[]>){
        this.notesSubject.subscribe(observer);
    }

    update():void{
        localStorage.setItem('notes', JSON.stringify(this.notes));
        this.notesSubject.next(this.notes.map(note => ({id:note.id, title:note.title})));
    }

    addNote(title:string, text:string):Note {
        const note = {id:this.nextId++, title, text};
        this.notes.push(note);
        this.update();
        return note;
    }

    updateNote(id:number, title:string, text:string) {
        const index = this.findIndex(id);
        this.notes[index] = {id, title, text};
        this.update();
    }

    getNote(id:number):Note{
        const index = this.findIndex(id);
        return this.notes[index];
    }

    deleteNote(id: number) {
        const index = this.findIndex(id);
        this.notes.splice(index, 1);
        this.update();
    }

    private findIndex(id: number):number{
        for (let i=0; i<this.notes.length; i++) {
          if (this.notes[i].id === id) return i;
        }
        throw new Error(`Note with id ${id} was not found!`);
    }

    ngOnDestroy():void{
        this.notesSubject.unsubscribe();
    }
}
