import { Component, OnInit } from '@angular/core';
import { Note } from '../class/Note';
import { NoteInfo } from '../class/NoteInfo';
import { NotesService } from '../service/notes.service';
import { BehaviorSubject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit
{
    notes = new BehaviorSubject<NoteInfo[]>([]);
    currentNote:Note = {id:-1, title:'', text:''};
    createNote:boolean = false;
    editNote:boolean = false;
    editNoteForm:FormGroup;

    constructor(private formBuilder:FormBuilder, private noteService:NotesService) { }

    ngOnInit() {
        this.noteService.subscribe(this.notes);
        this.editNoteForm = this.formBuilder.group({
            title:['', Validators.required],
            text:['', Validators.required],
        });
    }

    onSelectNote(id:number){
        this.currentNote = this.noteService.getNote(id);
    }

    noteSelected():boolean{
        return this.currentNote.id >= 0;
    }

    onNewNote():void{
        this.editNoteForm.reset();
        this.createNote = true;
        this.editNote = true;
    }

    onEditNote():void{
        if(this.currentNote.id < 0) return;
        this.editNoteForm.get('title').setValue(this.currentNote.title);
        this.editNoteForm.get('text').setValue(this.currentNote.text);
        this.createNote = false;
        this.editNote = true;
    }

    onDeleteNote():void{
        if(this.currentNote.id < 0) return;
        this.noteService.deleteNote(this.currentNote.id);
        this.currentNote = {id:-1, title:'', text:''};
        this.editNote = false;
    }

    updateNote():void{
        if(!this.editNoteForm.valid) return;

        const title = this.editNoteForm.get('title').value;
        const text = this.editNoteForm.get('text').value;
        if(this.createNote){
            this.currentNote = this.noteService.addNote(title, text);
        }else{
            const id = this.currentNote.id;
            this.noteService.updateNote(id, title, text);
            this.currentNote = {id, title, text};
        }
        this.editNote = false;
    }
}
