import { Component, Renderer2 } from '@angular/core';

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.css']
})
export class NavComponent
{
    constructor(private render:Renderer2) { }
    show:boolean = false;

    toggle(e:Event):void{
        e.preventDefault();
        this.show = !this.show;
        this.render.addClass(e.currentTarget, (this.show) ? "showNav" : "hideNav");
        this.render.removeClass(e.currentTarget, (this.show) ? "hideNav" : "showNav");
    }
}