import { NgModule } from '@angular/core';
import { ContentsComponent } from './contents.component';
import { MainComponent } from './main.component';
import { NavComponent } from './contents/nav.component';
import { DashComponent } from './contents/dash.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
      path: 'apps/dash', component: DashComponent, canActivate: []
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  declarations: [
      MainComponent, ContentsComponent, NavComponent, DashComponent
  ],
  exports : [
    MainComponent
  ],
  providers: [],
})

export class MainModule { }