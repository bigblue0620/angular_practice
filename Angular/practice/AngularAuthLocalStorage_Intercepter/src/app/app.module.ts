import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// used to create fake backend
import { fakeBackendProvider } from './_helpers';
import { BasicAuthInterceptor, ErrorInterceptor } from './_helpers';
import { HomeComponent, MainComponent } from './home';
import { LoginComponent } from './login';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        MainComponent
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

        // provider used to create fake backend
        fakeBackendProvider
    ],
    bootstrap: [AppComponent]
    })
export class AppModule { }
