import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ConfigService
{
    static PROTOCOL = 'http';
    static URL = '127.0.0.1';
    static PORT = '55030';
    
    static requestUrl(url:string){
        return `${ConfigService.PROTOCOL}://${ConfigService.URL}:${ConfigService.PORT}${url}`;
    }
}