﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';
import { ConfigService } from './config.service';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        console.log("UserService getAll");
        return this.http.get<User[]>(ConfigService.requestUrl('/users'));
    }
}