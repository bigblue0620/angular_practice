import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { ObserComponent } from './comp/obser.component';
import { ObserComponent2 } from './comp/obser2.component';

@NgModule({
    imports: [
        ReactiveFormsModule,
        CommonModule
    ],
    declarations: [
        ObserComponent, ObserComponent2
    ],
    exports : [
        ObserComponent, ObserComponent2
    ],
    providers: [],
})

export class ComponentModule { }