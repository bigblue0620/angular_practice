import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { merge } from 'rxjs/index';
import { map, takeUntil } from 'rxjs/operators'

@Component({
    selector: 'app-comp2',
    template: 
     `<button style='background-color:red' (click)="redBtn$.next()">
        red
      </button>
      <button style='background-color:green' (click)="greenBtn$.next()">
        green
      </button>
      <h1>lbl: {{ label$ | async}}</h1>`,
    styles: [
        `button {width:200px; height:50px;}
    `]
})

export class ObserComponent2 implements OnInit, OnDestroy{

    redBtn$ = new Subject<any>();
    greenBtn$ = new Subject<any>();
    label$ = new BehaviorSubject<string>('');

    sRed$:Observable<string>;
    sGreen$:Observable<string>;
    sColor$:Observable<string>;

    constructor(){
        this.sRed$ = this.redBtn$.pipe(map(u => 'red'));
        this.sGreen$ = this.greenBtn$.pipe(map(i => 'green'));
        this.sColor$ = merge(this.sRed$, this.sGreen$); 
    }

    ngOnInit():void{
        this.sColor$.subscribe(this.label$);
    }

    ngOnDestroy():void{
        this.redBtn$.unsubscribe();
        this.greenBtn$.unsubscribe();
        this.label$.unsubscribe();
    }

}
  