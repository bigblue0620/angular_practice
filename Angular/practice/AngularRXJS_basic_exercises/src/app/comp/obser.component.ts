import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, fromEvent, from, Subscription, of, throwError } from 'rxjs';
import { map, filter, tap, debounceTime, switchMap, catchError } from 'rxjs/operators';

interface GithubUser {
    login:number;
    name:string;
}

@Component({
    selector: 'app-comp1',
    templateUrl: './obser.component.html',
    styleUrls: []
})

export class ObserComponent implements OnInit, OnDestroy, AfterViewInit
{
    mousePosition$:Observable<Event>
    posX:number = 0;
    posY:number = 0;

    myArr:number[] = [1,2,3,4,5];
    result:number[] = [];
    arrSubscription:Subscription;

    //searchInput:FormControl =  new FormControl('');
    searchInput:FormControl;
    githubUser:GithubUser;
    gitSubscription:Subscription;

    test:string = 'abcd';
    
    constructor(private http:HttpClient){
        this.searchInput =  new FormControl('');
    };

    ngOnInit():void{
        this.mousePosition$ = fromEvent(document, 'mousemove');
        this.mousePosition$.subscribe(
            (event:MouseEvent) => {
                this.posX = event.clientX;
                this.posY = event.clientY;
            },
            error => console.log(error),
            () => console.log('complete mouse')
        );

        const myArr$:Observable<any> = from(this.myArr);
        this.arrSubscription = 
            myArr$.pipe(
                map((item:number) => item * 2),
                filter((item:number) => item > 5),
                tap(item => console.log(item))            
            ).subscribe(
                value => this.result.push(value),
                error => console.log(error),
                () => console.log('complete arr')
            );

        this.gitSubscription = this.searchInput.valueChanges
            .pipe(
                tap((data:string)=>console.log(data)),
                //옵저버블이 방출하는 데이터를 수신하는 시간을 지연
                debounceTime(500),
                //새로운 옵저버블을 생성함
                tap((data:string)=>console.log('start', data)),
                switchMap((userId:string) => this.getGithubUser(userId))
            ).subscribe(
                user => this.githubUser = user,
                error => console.log(error),
                () => console.log('complete git')
            )
        
    }

    ngAfterViewInit():void{
        
    }

    getGithubUser(userId:string):Observable<GithubUser>{
        return this.http.get<GithubUser>(`https://api.github.com/users/${userId}`)
            .pipe(
                map(user => ({login:user.login, name:user.name})),
                tap(console.log),
                catchError((err:HttpErrorResponse) => {
                    if(err.status === 404){
                        return of(`[ERROR] Not found user: ${userId}`);
                    }else{
                        return throwError(err);
                    }
                })
            )
    }

    ngOnDestroy():void{
        this.arrSubscription.unsubscribe();
        this.gitSubscription.unsubscribe();
    }
}