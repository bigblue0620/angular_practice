import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
//import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ComponentModule } from './component.module';
//import { ObserComponent } from './comp/obser.component';

@NgModule({
  declarations: [
    AppComponent,
    //ObserComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    //FormsModule,
    //ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    ComponentModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
