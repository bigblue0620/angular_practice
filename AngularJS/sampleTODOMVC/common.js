var app = angular.module("MyApp", []);

app.controller("todosMvc", function($scope)
{
	$scope.items = [];

	$scope.completedCount = $scope.items.length;

	$scope.clearAll = function()
	{
		$scope.completedCount = $scope.items.length;
		angular.forEach($scope.items, function(item) {
			item.status = false;
		});
	};

	$scope.changeStatus = function(item)
	{
		item.status = !item.status;
		if(item.status == true){
			$scope.completedCount -= 1;
		}else{
			$scope.completedCount += 1;
		} 
	};

	$scope.existCompletedItem = function()
	{
		var result = false;
		($scope.items).some(function (item, index) {
			if(item.status == true){
				result = true;
			}
		});
		
		return result;
	};

	$scope.deleteItem = function(item, index)
	{
		($scope.items).splice(index, 1);
	};
	
	$scope.addItem = function(event)
	{
		if (event.which === 13){//enter 키 입력시
		    $scope.items.push({title : $scope.newItem, status : false});
		    $scope.newItem = "";
		    $scope.completedCount++;
		}
	};
});