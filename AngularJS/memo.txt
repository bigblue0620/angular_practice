Angular의 모든 서비스는 싱글턴 패턴이다. .service()메소드와 .factory() 메소드의 차이는 객체를 생성하는 방법에서 차이가 있다.

서비스: 생성자 함수와 같이 동작하고 new 키워드를 사용해 인스턴스를 초기화 한다. 서비스는 퍼블릭 메소드와 변수를 위해 사용한다.
function SomeService () {
  this.someMethod = function () {
    // ...
  };
}
angular
  .module('app')
  .service('SomeService', SomeService);
  
팩토리: 비지니스 로직 또는 모듈 제공자로 사용한다. 객체나 클로저를 반환한다.
객체 참조에서 연결 및 갱신을 처리하는 방법으로 인해 노출식 모듈 패턴(Revealing module pattern) 대신 호스트 객체 형태로 반환한다.

function AnotherService () {
  var AnotherService = {};
  AnotherService.someValue = '';
  AnotherService.someMethod = function () {
    // ...
  };
  return AnotherService;
}
angular
  .module('app')
  .factory('AnotherService', AnotherService);
  
  서비스와 팩토리에서 가장 두드러진 차이점을 꼽는다면, 서비스에서는 초기화 과정이 존재하기 때문에 자연스럽게 prototype 상속이 가능하다. 
  그래서 일반적으로 상속이 필요한 데이터 핸들링이나 모델링 등의 경우에는 서비스를 활용하고, helper나 정적 메소드와 같이 활용되는 경우는 팩토리로 구현을 많이 하는 것 같다.
  

//angular 에서 기존 자바스크립트 모델 오브젝트는?  
  https://medium.com/opinionated-angularjs/angular-model-objects-with-javascript-classes-2e6a067c73bc#.30f9fh1s8
  Unlike Backbone and Ember, AngularJS does not provide a standardized way to define model objects. 
  The model part of MVC* in Angular is the scope object, which is not what we mean by model objects. 
  A model object is the JavaScript version of a class instance. 
  Developers familiar with object oriented programming will feel right at home using model objects.
  
  
 $timeout 에서 바깥의 변수를 참조할 경우 
Usually you don't have to pass parameters explicitly because they are defined in the enclosing scope and are visible in the closure. This works as well:

var param = {num:9};
$timeout(function(){
  console.log(param.num);
});

If you need to pass a parameter explicitly -- usually because you have to preserve the value of some changing variable --, the answer of @Pankaj-Parkar provides a good workaround. However, I like it better this way:

$timeout((function(test) {
    return function() { console.log(test.num) }
})({num: 9}));

/*if(!$rootScope.$$listenerCount['updateChatee']){//디렉티브 생성 수만큼 on 리스너가 붙을 수 있으므로 이렇게 리스너 카운트를 볼 필요가 있음. 
		$scope.$on("updateChatee", function()
	    {
			
		});
	}*/