app.controller("spa-shell-chat", function($scope, $location, User, EncodeHtml)
{
	$scope.userList = [];
	$scope.config = {
		extended_height : 240,
		extended_head : "=",
		default_height : 25,
		default_head : "+"
	};

	$scope.heightAni = {low:true, high:false};
	$scope.sliderHead = $scope.config.default_head;
	$scope.chatTitle = "chat";

	$scope.toggleSlider = function(event)
	{
		if(!User.isLoginState()){
			console.log("Not User Login State");
			return;
		}

		if(!event){//back 버튼 클릭 시
			if(($location.hash() == 'open') || arguments[1] == 'open'){ //$location.hash(); //이동해야할 화면(채팅 슬라이더 Open/Close 해시값)
				$scope.heightAni={low:false, high:true};
				$scope.sliderHead = $scope.config.extended_title;
				$location.hash('open');
			}else{
				$scope.heightAni={low:true, high:false};
				$scope.sliderHead = $scope.config.default_title;
				$location.hash('closed');
			}
			return;
		}

		if((event.currentTarget).offsetHeight == $scope.config.extended_height){
			$scope.heightAni={low:true, high:false};
			$scope.sliderHead = $scope.config.default_head;
			$location.hash('closed');
		}else{
			$scope.heightAni={low:false, high:true};
			$scope.sliderHead = $scope.config.extended_head;
			$location.hash('open');
		}
	};

	$scope.$on('closeChatSlider', function(){
		$scope.closeSlider();
	});

	$scope.closeSlider = function()
	{
		$scope.heightAni={low:true, high:false};
		$scope.sliderHead = $scope.config.default_head;
		$location.hash('closed');
	};

	$scope.$on("loginComplete", function()
	{
		$scope.userList = User.getChatUserList();
		$scope.toggleSlider(null, 'open');
	});

	$scope.updateChat = function(arg)
	{
		angular.element(".spa-chat-msgs").append(arg.msg);
		$scope.scrollChat();
		$scope.chatMsg = "";
	};

	$scope.writeChatByMe = function()
	{
		if(!$scope.chatMsg)
			return;

		var arg = {
			dest_id : User.getChatee().getID(),
			dest_name : User.getChatee().getName(),
			sender_id : User.getUser().getID(),
			msg : EncodeHtml.getMsgForm(User.getUser().getName() + " : " + $scope.chatMsg)
		}

		$scope.updateChat(arg);

        //TODO 채팅문자를 보내면 상대방으로부터 자동적으로 답변이 오도록 코딩해 둠
        setTimeout(function(){
            var arg = {
    			dest_id : User.getUser().getID(),
    			dest_name : User.getUser().getName(),
    			sender_id : User.getChatee().getID(),
    			msg : EncodeHtml.getLogForm(User.getChatee().getName() + ' : Thanks for the note, ' + User.getUser().getName())
    		}
            $scope.updateChat(arg);
        }, 2000);
	};

	$scope.scrollChat = function()
	{
		var chat = angular.element(".spa-chat-msgs");
		chat.animate({scrollTop : chat.prop('scrollHeight') - chat.height()}, 150);
	};
})
.directive('divUser', function(User, $timeout, EncodeHtml){
	return {
    	restrict : 'E',
    	replace : true,
        transclude : true,
        scope : false,
        controller : "spa-shell-chat",
        template : "<div data-id={{user.id}} data-name={{user.name}}>{{user.name}}</div>",
        link: function(scope, element, attr, controller)
        {
            element.bind('click', function(){
                var result = User.setChatee(attr.id);

                if(result === true){
                    angular.element("#spa-chat-box-input").focus();
                    angular.element(".spa-chat-list-name").removeClass('spa-x-select');
                    element.addClass('spa-x-select');

                    scope.$parent.chatTitle = "Chat with " + attr.name;
                    scope.$parent.updateChat({msg : EncodeHtml.getAlertForm(attr.name)});
                }
            });
        }
    };
});
