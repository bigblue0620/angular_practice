app.controller("spa-shell-main", function($scope, $rootScope, User)
{
	$scope.userList = [];

	$scope.$on("loginComplete", function()
    {
		$scope.userList = User.getUserList();
	});
})
.directive('avtrUser', function(User){
	return {
    	restrict : 'E',
    	replace : true,
        transclude : true,
        scope : false,
        controller : "spa-shell-main",
        template : "<div data-id={{user.id}} data-name={{user.name}} style={{user.css_map}}>{{user.name}}</div>",
        link: function($scope, $element, $attr, $controller, $timeout)
        {
        	var gabX, gabY, pressObj;
        	
        	if($attr.id == User.getUser().getID()){
        		$element.addClass('spa-x-is-user');
        	}

        	$element.bind('mousedown', function(e) {
        		$timeout(function() {
                    if(!pressObj){
                    	gabX = e.pageX - $element.offset().left;
                    	gabY = e.pageY - $element.offset().top + 40;
                    	pressObj = $element;
                    	$element.addClass('spa-x-is-drag');
                    }
                }, 1000);
            });
        	
        	$element.bind('mouseout', function(e) {
                $element.removeClass('spa-x-is-drag');
                pressObj = null;
            });

        	$element.bind('mousemove', function(e) {
        		if(pressObj == $element){
                	var x = e.pageX - gabX;
        			var y = e.pageY - gabY;
                	
                    $element.css({'top' : y, 'left' : x});
        		}
            });

        	$element.bind('mouseup', function(e) {
                $element.removeClass('spa-x-is-drag');
                pressObj = null;
            });
        	
        	$scope.$on("updateChatee", function()
        	{
        		if($attr.id == arguments[1].msg){
        			$element.addClass('spa-x-is-chatee');
        		}else{
        			$element.removeClass('spa-x-is-chatee');
        		}
        	});
        }
    };
});