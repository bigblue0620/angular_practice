app.factory('User', function($timeout, $rootScope)
{
	var config = {anon_id : 'a0', cid_serial : 0, people_db : TAFFY(), dummy : true, fakeId_serial : 5};
	var stateMap = {userList : []};
	var currentUser = null;
	var currentChatee = null;
	var fakeUsers = [{name : 'Betty', 	_id : 'id_01', css_map : 'top:20px;left:20px;background-color:rgb(128, 128, 128);'},
	 		         {name : 'Mike', 	_id : 'id_02', css_map : 'top:70px;left:20px;background-color:rgb(128, 255, 128);'},
			         {name : 'Pebbles', _id : 'id_03', css_map : 'top:120px;left:20px;background-color:rgb(128, 192, 192);'},
			         {name : 'Wilma', 	_id : 'id_04', css_map : 'top:170px;left:20px;background-color:rgb(192, 128, 128);'}];
	
	function User(arg)
	{
		this.name = arg.name;
		this.cid = arg.cid;
		this.id = arg.id;
		this.css_map = arg.css_map;
	};

	User.prototype.getID = function(){
		return this.id;
	};

	User.prototype.getName = function(){
		return (this.name || this.id);
	};

	/*User.prototype.getInfo = function(){
		return {cid : this.cid, id : this.id, name : this.name, css_map : this.css_map}; 
	};*/

	_makeCid = function(){
		return 'c' + String(config.cid_serial++);
	};
	
	init = function()//fakeUsers를 사용할 경우 Taffiy DB에 넣어서 관리, 초기 기동시 처리함
	{
		if(config.dummy && config.people_db().count() == 0){
			for(i=0; i<fakeUsers.length; i++){
				var tmpUser = fakeUsers[i];
				_makeUser({cid : tmpUser._id, id : tmpUser._id, name : tmpUser.name, css_map : tmpUser.css_map});
			}
		}
	};
	
	_makeUser = function(arg)
	{
		var usr = new User(arg);
		
		stateMap.userList.push(usr);
		config.people_db.insert(usr);
		return usr;
	};

	login = function(userId)
	{
		init();
		
		if(!userId)
			throw new Error("userID empty");
		
		var arg = {cid : _makeCid(), id : userId, name : userId, css_map : {top : 25, left : 25, 'background-color' : '#8f8'}};
		currentUser = _makeUser(arg);

		$timeout(function(){
			$rootScope.$broadcast('loginLogoutEvent', {msg : userId});
		}, 700);
	};

	logout = function(){
		currentUser = null;
		config.people_db = TAFFY();
		stateMap.userList = [];
		
		$timeout(function(){
			$rootScope.$broadcast('loginLogoutEvent', {msg : ""});
			$rootScope.$broadcast('closeChatSlider');
		}, 500);
	};

	isLoginState = function(){
		return (currentUser != null);
	};
	
	getUser = function(id)
	{
		if(!id)
			return currentUser;
		
		return stateMap.userList.filter(function(item){
			return (item.id == id);
		})[0]; 
	};

	getChatUserList = function()
	{
		if(config.dummy){
			var tmp = stateMap.userList.filter(function(item){
				return (item.id != currentUser.id);
			});
			return tmp;
		}else{
			//TODO 실제 통신 등으로 취득한 유저 리스트(실제 서버 통신등으로 취득, 별도의 서비스 등으로 정의) 를 리턴
		}
	};
	
	getUserList = function()
	{
		if(config.dummy){
			return stateMap.userList;
		}else{
			//TODO 실제 통신 등으로 취득한 유저 리스트(실제 서버 통신등으로 취득, 별도의 서비스 등으로 정의) 를 리턴
		}
	};
	
	setChatee = function(chateeId)
	{
		var oldChatee = currentChatee;
		var newChatee = getUser(chateeId);

		if(oldChatee == newChatee){
			return false;
		}

		currentChatee = newChatee;

		$rootScope.$broadcast('updateChatee', {msg : chateeId});

		return true;
	};
	
	getChatee = function()
	{
		if(!currentChatee)
			return false;
		
		return currentChatee;
	};

	return {
		init  : init,
		login : login,
		logout : logout,
		isLoginState : isLoginState,
		getUserList : getUserList,
		getChatUserList : getChatUserList,
		setChatee : setChatee,
		getChatee : getChatee,
		getUser : getUser
	}
});