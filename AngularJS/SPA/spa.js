var app = angular.module("spa", ['ngRoute']);

app.config(['$locationProvider', function($locationProvider){
    $locationProvider.html5Mode(true).hashPrefix('!');
}]);

app.config(['$compileProvider', function ($compileProvider) {
	$compileProvider.debugInfoEnabled(true);
}]);

//Use this method to register work which should be performed when the injector is done loading all modules.
app.run(function($rootScope, $route, $location, User)
{
	User.init();

	$rootScope.$on('$locationChangeSuccess', function(){
		$rootScope.actualLocation = $location.url();
    });

	$rootScope.$watch(function () {return $location.url()}, function (newLocation, oldLocation){
		if($rootScope.actualLocation === newLocation || !$rootScope.actualLocation && $location.hash()) {//back 버튼 클릭 || 처음 로딩시 url 자체에 해시 태그가 있는 경우
			var chatSliderScope = angular.element(document.getElementById('spa-shell-chat')).scope();
			chatSliderScope.toggleSlider();
		}
    });
});

app.controller("spa-shell-login", function($scope, $rootScope, $timeout, User)
{
	$scope.msg = "Please sign-in";

	$scope.loginLogoutProc = function()
	{
		if(User.isLoginState()){//logout 함.
			User.logout();
		}else{
			var userId = prompt($scope.msg);
			User.login(userId);

			$timeout(function(){
				$scope.msg = "processing...";
			}, 300);
		}
	};

	$scope.$on('loginLogoutEvent', function(event, arg)
    {
		$scope.msg = (arg.msg) ? arg.msg : $scope.msg;

		if(arg.msg){
			$rootScope.$broadcast('loginComplete');
		}
	});
});

app.service('EncodeHtml', function()
{
	return {
		getMsgForm : function(msg){
			return '<div class="spa-chat-msg-log-me">' + msg + '</div>';
		},
		getLogForm : function(msg){
			return '<div class="spa-chat-msg-log-msg">' + msg + '</div>';
		},
		getAlertForm : function(msg){
			return '<div class="spa-chat-msg-log-alert">Now chatting with '+ msg + '</div>';
		}
	};
});
