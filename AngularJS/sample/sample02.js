angular.module('appModule', [])  
   .controller('Ctrl', function($scope) {
      $scope.items = [
          {title: 'What is Directive?',
           content: 'Dom element'},
          {title: 'Custom Directive',
           content: 'Make Directive'},
          {title: 'Bye~',
           content: 'end'}
      ];
   })
   .directive('myTitle', function() { //<my-title> 태그
       return {
          restrict: 'E',
          replace: true,
          transclude: true,
          template: '<div ng-transclude></div>',
          controller: function() {
        	 console.log(items);
             var items = [];
             this.addItem = function(item) {
                items.push(item);
                //console.log(items.length);
             }
         }
      };
   })
   .directive('myContent', function(){//<my-content ~ 태그
       return {
           restrict: 'E',
           replace: true,
           transclude: true,
           require: '^?myTitle',//다른 컨트롤러나 디렉티브의 controller()에 this로 정의된 function을 사용할 때 선언. 
           scope: { title:'=itemTitle', content:'=itemContent' },//<my-content class='table' ng-repeat='item in items' item-title='item.title'  <---이렇게 해줘야 {{content}}가 동작함.
           template : '<div>' +
                      '<div class="title" ng-click="click()">{{title}}</div>' +
                      '<div class="body" ng-show="showMe">{{content}}</div>' + //ng-transclude 여기서 {{item.content}}가 체인지됨
                      '</div>',
           link: function(scope, element, attrs, controller) {
        	   console.log(controller);
               scope.showMe = false; //ng-show="showMe"
               controller.addItem(scope);
               scope.click = function click(){
                  scope.showMe = !scope.showMe;
               }
           }
       };
   });

/*

http://www.nextree.co.kr/p4850/

3.7 scope

디렉티브의 scope를 설정.

3.7.1 scope 옵션

scope : false -> 새로운 scope 객체를 생성하지 않고 부모가 가진 같은 scope 객체를 공유. (default 옵션)
scope : true -> 새로운 scope 객체를 생성하고 부모 scope 객체를 상속.
scope: { ... } -> isolate/isolated scope를 새롭게 생성.
scope: { ... }는 재사용 가능한 컴포넌트를 만들 때 사용하는데 컴포넌트가 parent scope의 값을 read/write 못하게 하기 위함입니다. parent scope에 접근(access) 하고 싶을 경우 Binding 전략(=, @, &)를 이용합니다.
 
 
***********위의 예에서 scope를 주석 처리하면 {{item.title}} {{item.content}}로 써야 함.

html 파일의 <!-- {{item.content}} --> 주석 처리하고
{{content}}이렇게만 쓸 경우에는 <my-content class='table' ng-repeat='item in items' item-title='item.title'  item-content="item.content" <---이렇게 해줘야 {{content}}가 동작함.

즉 html 태그 안에서 item-title, item-content가 정의되어 있고
scope에 { title:'=itemTitle', content:'=itemContent' } 일케 되어 있어야 함.


<div class="body" ng-show="showMe" ng-transclude></div>
실제 렌더링은 
<div class="body ng-hide" ng-show="showMe" ng-transclude=""><span class="ng-binding ng-scope">Dom element</span></div>

ng-hide 또한 AngularJS와의 사전 정의 된 CSS 클래스이며, 요소의 설정 display 에 none .

When the ngShow expression evaluates to a falsy value then the .ng-hide CSS class is added to the class attribute on the element causing it to become hidden.

*/