var App01 = angular.module('MyApp01', []);

App01.controller('MainCtrl01', ['$scope', function ($scope)
{
    $scope.nameFirst = "John";
    $scope.nameLast = "Doe";
    
    $scope.onkeyUp = function(){
    	console.log($scope.nameFirst);
    };
}])
.directive('customButton', function () {
	return {
	    restrict: 'A', //DOM 엘리먼트의 속성을 설정.  A : attribute, E : element, C : class, M : comment 생략시는 A
	    replace: true, //디렉티브를 사용한 HTML의 태그에 template 또는 templateUrl에 포함된 태그 내용을 추가할지 교체할지 설정. 
	    transclude: true,//ng-transclude를 이용하여 template 또는 templateUrl에서 디렉티브내의 원본내용을 포함시킬지 설정. 
	    template: '<a href="" class="myawesomebutton" ng-transclude>' +
	                '<i class="icon-ok-sign"></i>' +
	              '</a>',
	    link: function (scope, element, attrs, controller) {//2-way data binding을 위해 해당 디렉티브 DOM엘리먼트의 event listener를 등록. 
	    	//DOM 조작과 이벤트 설정은 여기서!
	    	//alert("Test"); <- 일케 하면 로딩시 alert 이 불려짐
	    	
	    	//scope.showMe = false;
	    	//controller.addItem(scope);
	    	//scope.click = function click(){
        	//}
	    }
		//require, controller 등은 다른 디렉티브와 통신(?)하기 위해 필요한 속성임.
	};
});