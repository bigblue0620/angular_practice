angular.module('exampleDirective', [])  
  .controller('Ctrl', function($scope) {
    $scope.person = {
      name: 'nextreeMember',
      address: 'Gasan'
    };
  })
  .directive('myExample', function() {
    return {
      restrict: 'E',
      template: '<div>Name: {{person.name}} </br> Address: {{person.address}} </br> <span ng-transclude></div>',
      transclude : true //ng-transclude를 이용하여 template 또는 templateUrl에서 디렉티브내의 원본내용을 포함시킬지 설정. 
      					//true로 설정 시 디렉티브에 포함된 원본내용을 template의 ng-transclude를 사용한 곳으로 포함합니다. - 위의 <span ng-transclude>에 들어감. 
    };
});