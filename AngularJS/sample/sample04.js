var myApp = angular.module('appModule', []);

myApp.controller('Ctrl', function Ctrl($scope, $http)
{
	// 사용자 객체를 생성
	$scope.user = {};
	
	// No data
	$scope.main = {};
    $scope.main.test1 = [];
    
    // We have data!
    $scope.main.test2 = "sdfsdfsdfs";

    $http({
	    method: 'GET',
	    url: 'http://localhost:1337/test'
	}).success(function (data, status, headers, config) {
		console.log("http request succeed");
	    console.log(data);
	    $scope.user.username = data.user.name;
	}).error(function (data, status, headers, config) {
		console.log("http request failed");
		console.log(data);
		console.log(status);
	});
	
});

myApp.controller('EmailsCtrl', ['$scope', function ($scope) {

	  // 이메일 객체를 생성
	  $scope.emails = {};

	  // 서버에서 데이터를 받아온 것처럼 꾸며보자. 
	  // 그냥 객체의 배열이다.
	  $scope.emails.messages = [{
	        "from": "Steve Jobs",
	        "subject": "I think I'm holding my phone wrong :/",
	        "sent": "2013-10-01T08:05:59Z"
	    },{
	        "from": "Ellie Goulding",
	        "subject": "I've got Starry Eyes, lulz",
	        "sent": "2013-09-21T19:45:00Z"
	    },{
	        "from": "Michael Stipe",
	        "subject": "Everybody hurts, sometimes.",
	        "sent": "2013-09-12T11:38:30Z"
	    },{
	        "from": "Jeremy Clarkson",
	        "subject": "Think I've found the best car... In the world",
	        "sent": "2013-09-03T13:15:11Z"
	    }];
	  
	  $scope.deleteEmail = function (index) {
		  $scope.emails.messages.splice(index, 1)
	  };

	}]);