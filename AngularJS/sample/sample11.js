var app = angular.module("demo", []);

app.controller("dropdownDemo", function($scope) {
	$scope.colours = [{
		name: "Red",
		hex: "#F21B1B"
	}, {
		name: "Blue",
		hex: "#1B66F2"
	}, {
		name: "Green",
		hex: "#07BA16"
	}];
	$scope.colour = ""; //이건 <b style="color: {{colour.hex}}">{{colour.hex}}</b> 와 연동
});

app.run(function($rootScope) {
	angular.element(document).on("click", function(e) {
		console.log("click");
		$rootScope.$broadcast("documentClicked", angular.element(e.target));
	});
});

app.directive("dropdown", function($rootScope) {
	return {
		restrict: "E",
		templateUrl: "sample11_templates_dropdown.html",
		scope: {
			//@ : 디렉티브의 attribute value를 {{}}방식(interpolation)을 이용해 부모 scope에 접근
			//= : 부모 scope의 property와 디렉티브의 property를 data binding하여 부모 scope에 접근
			placeholder: "@",   //<dropdown placeholder="Colour..." list="colours" property="name" selected="colour">
			list: "=",
			selected: "=",
			property: "@"
		},
		/*

	<div class="dropdown-display" ng-click="show();" ng-class="{ clicked: listVisible }">  풀다운 버튼
        <span ng-if="!isPlaceholder">{{display}}</span>
		<span class="placeholder" ng-if="isPlaceholder">{{placeholder}}</span> <--------- 젤 처음 표시됨, 
		그 후 선택된게 있으면 scope.select 역시ㅓ isPlaceHolder가 false가ture가 되면서 위에 <span ng-if="!isPlaceholder">{{display}}</span>이게 표시됨.  
        <i class="fa fa-angle-down"></i>
    </div>

	<div ng-repeat="item in list" ng-click="select(item)" ng-class="{ selected: isSelected(item) }">
		<span>{{property !== undefined ? item[property] : item}}</span>
        <i class="fa fa-check"></i>
	</div>


		 */
		link: function(scope) {
			scope.listVisible = false;
			scope.isPlaceholder = true;

			scope.select = function(item) {
				console.log("sel");
				scope.isPlaceholder = false;
				scope.selected = item;
			};

			scope.isSelected = function(item) {
				return item[scope.property] === scope.selected[scope.property];
			};

			scope.show = function() {
				scope.listVisible = true;
			};

			$rootScope.$on("documentClicked", function(inner, target) {
				console.log($(target[0]).is(".dropdown-display.clicked") || $(target[0]).parents(".dropdown-display.clicked").length > 0);
				if (!$(target[0]).is(".dropdown-display.clicked") && !$(target[0]).parents(".dropdown-display.clicked").length > 0)
					scope.$apply(function() {
						scope.listVisible = false;
					});
				
				/*console.log("-------------- " + scope.placeholder);
				console.log(scope.list);
				console.log(scope.selected);
				console.log(scope.property + "-----------------");*/
			});

			scope.$watch("selected", function(value) {  //이 함수는 로딩할 때 반드시 1번 실행됨
				console.log(arguments);
				//console.log("selected : " + JSON.stringify(value));
				scope.isPlaceholder = scope.selected[scope.property] === undefined;
				scope.display = scope.selected[scope.property];
				/*console.log("scope.isPlaceholder : " + scope.isPlaceholder);
				console.log("scope.display : " + scope.display);
				console.log(scope.selected);*/
			});
		}
	}
});