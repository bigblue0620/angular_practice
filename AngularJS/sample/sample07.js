angular.module('exampleDirective', [])  
  .controller('Ctrl', function($scope) {
    $scope.person = {
      name: 'nextreeMember',
      address: 'Gasan'
    };
  })
  .directive('myExample', function() {
    return {
      restrict: 'E',
      template: '<div>Name: {{person.name}} </br> Address: {{person.address}}</div>',
      replace: true  //<my-example></my-example> 태그가 위의 태그로 대체됨
    };
});