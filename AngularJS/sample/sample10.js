/*
 * 3.7.2 Binding 전략

	= : 부모 scope의 property와 디렉티브의 property를 data binding하여 부모 scope에 접근
	@ : 디렉티브의 attribute value를 {{}}방식(interpolation)을 이용해 부모 scope에 접근
 */
angular.module('exampleDirective', [])  
  .controller('Ctrl', function($scope) {
    $scope.name = 'Nextree';
    $scope.locate = 'Gasan';
  })
  .directive('nextreeDirective', function() {
    return {
      restrict: 'E',
      scope: {
        name: '@companyInfo'
      },
      transclude : true,
      template: 'Name:{{name}} <br><span ng-transclude>'
    };
  });