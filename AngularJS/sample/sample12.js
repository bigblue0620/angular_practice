var app = angular.module("demo", []);

app.controller("serviceForm", function($scope) {
	$scope.services = [{
		name : "Web Development",
		value : "300.00"
	}, {
		name : "Design",
		value : "400.50"
	}, {
		name : "Integration",
		value : "250.50"
	}, {
		name : "Training",
		value : "220.00"
	}];
	
	$scope.selectedServices = [];
	$scope.total = 0;
	
	$scope.isSelected = function(service){
		return $scope.selectedServices.includes(service);
	}
	
	$scope.select = function(index){
		//var tmp = angular.copy($scope.services[index]); //이렇게 deep copy하면 includes가 다른 객체로 판단해서 false 리턴
		var tmp = $scope.services[index];

		if(this.isSelected(tmp)){
			var idx = $scope.selectedServices.indexOf(tmp);
			$scope.selectedServices.splice(idx, 1);
		}else{
			$scope.selectedServices.push(tmp);
		}
		
		console.log(JSON.stringify($scope.selectedServices));
		//console.log(JSON.stringify($scope.services));
		
		$scope.total = $scope.getTotal();//여기서 total을 갱신하면 화면에 바로 갱신값이 반영됨. angular 장점이라 봄.
	}
	
	$scope.getTotal = function()
	{
		var tmp = 0;
		/*for(var i=0; i<$scope.selectedServices.length; i++){
			tmp += parseFloat($scope.selectedServices[i].value);
		}*/
		
		angular.forEach($scope.selectedServices, function(item){ //angular 쓰니까 angular 의 forEach를 쓰자 ㅋㅋ
			tmp += parseFloat(item.value);
		});
		
		//return (tmp).toFixed(2);
		return tmp; //angular filter 적용함.
	}
});