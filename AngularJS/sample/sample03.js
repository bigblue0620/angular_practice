/*
서비스

서비스는 종종 헷갈리는 부분이다. 경험에 비춰보면 서비스는 기능적인 큰 차이점을 제공하지 않으면서도 뭔가 더 좋아보이는 디자인 패턴이다. 
Angular 소스를 분석해보니 Angular는 같은 컴파일러를 사용하면서 많은 기능을 제공하는듯 하다. 분석해보니 서비스는 싱글톤 으로 사용해야하고 
객체 리터럴이나 좀 더 복잡한 유즈 케이스처럼 더 복잡한 기능은 팩토리를 사용해야 한다.

서비스(혹은 팩토리)를 생성할때는 의존성 주입을 사용해서 Angular에게 새로 만든 서비스의 존재를 알려줘야 한다. 
알려주지 않으면 컴파일 에러가 발생하거나 컨트롤러가 동작하지 않을 것이다. 
컨트롤러 선언부분에 function ($scope) 를 봤을텐데 이게 바로 간단한 의존성 주입 방법이다. 
function ($scope) 앞에 있는 [‘$scope’] 도 봤겠지만 이건 나중에 설명하겠다. 다음 예제는 의존성 주입을 통해 Angular에게 서비스가 필요하다고 알려주는 방법이다:


*/



var myApp = angular.module('appModule', []);

myApp.service('MyMath', function () {
	this.multiply = function (x, y) {
		return x * y;
	};
});

myApp.factory('MyStrReverse', function()
{
	return {
		reverse : function(name){
			return name.split("").reverse().join("");
		}
	}
});

myApp.filter('filterTest', function () {
    return function (input, uppercase) {
    	var out = '';
        for (var i = 0; i < input.length; i++) {
            out = input.charAt(i) + out;
        }
        if (uppercase) {
            out = out.toUpperCase();
        }
        return out;
    }
});

myApp.filter('oddNumbers2', function () { //이건 {{ 4 | oddNumbers2 }} 이렇게 표현식으로 쓸 때 사용하는 문법
    return function (num) {
    	console.log("odd num : " + num);
        if(num % 2 == 1) return num;
        else return num + " isn't odd";
    }
});

//myApp.controller('Ctrl', ['$scope', 'MyMath', 'MyStrReverse', function ($scope, MyMath, MyStrReverse) {
myApp.controller('Ctrl', function Ctrl($scope, MyMath, MyStrReverse) {//이런 방식도 있음.
    var a = 2;
    var b = 4;
    
    $scope.result = "";
    $scope.name = "chone";
    $scope.greeting = 'Todd Motto';
    $scope.myNumbers = [10, 25, 35, 45, 60, 80, 100];
    
    // 결과는 288
    $scope.result = MyMath.multiply(a, b);
    
    $scope.reverseNameFactory = function()
	{
		$scope.name = MyStrReverse.reverse($scope.name);  
	};
	
	$scope.oddNumbers = function (num){ //<li ng-repeat="number in myNumbers | filter:oddNumbers"> 이렇게 디렉티브와 같이 쓸 때는 컨트롤러 안에 선언해야 함.
	    return (num % 2 == 1);
	};
});


/*

ECMAScript 5 

map() 메소드는 배열 내의 모든 요소 각각에 대하여  제공된 함수(callback)를 호출하고, 그 결과를 모아서,  새로운 배열을 반환합니다.

var numbers = [1, 4, 9];
var roots = numbers.map(Math.sqrt);
// roots의 값은 [1, 2, 3]이 되지만, numbers는 그대로 [1, 4, 9]입니다.
 */
var a = [
         "Hydrogen",
         "Helium",
         "Lithium",
         "Beryl­lium"
       ];

var a2 = a.map(function(s){ return s.length });
console.log(a2);


var a3 = a.map( s => s.length );
console.log(a3);

/*

// ES5
var selected = allJobs.filter(function (job) {
  return job.isSelected();
});

// ES6
var selected = allJobs.filter(job => job.isSelected());



*/