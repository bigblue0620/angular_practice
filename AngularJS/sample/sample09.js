angular.module('exampleDirective', [])  
  .controller('Ctrl', function($scope) {
	  $scope.nextree = { name: 'NextreeAAA'};
  })
  /*.directive('nextreeDirective', function() {
    return {
      restrict: 'E',
      scope: {
        myCompany: '=companyInfo'
      },
      template: 'Name:{{myCompany.name}}'
    };
  });*///이 경우 html 태그는 <nextree-directive company-info="nextree"></nextree-directive> 이고 company-info가 꼭 있어야 함.
  .directive('nextreeDirective', function() {
    return {
      restrict: 'E',
      template: 'Name:{{nextree.name}}'
    };
  });//이 경우 <nextree-directive></nextree-directive> 이렇게 OK //scope : false -> 새로운 scope 객체를 생성하지 않고 부모가 가진 같은 scope 객체를 공유. (default 옵션)